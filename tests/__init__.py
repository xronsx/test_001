# Flask libraries
# 3rd party libraries
# Standard/core python libraries
from unittest import TestCase
import os
# Our custom libraries
from core.database import Connector
from application import application

os.environ['TEST'] = "True"
database = "TEST_" + os.environ.get("MONGO_DATABASE", "default")
os.environ['MONGO_DATABASE'] = database


# BASE TestCase TESTS
class BaseTestClass(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        application.testing = True
        cls.client = application.test_client()

    @classmethod
    def tearDownClass(cls):
        connector = Connector()
        connector.mongoClient.drop_database(database)
        super().tearDownClass()
