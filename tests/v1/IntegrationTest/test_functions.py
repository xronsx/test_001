import unittest
import os
import pytest
import shutil
from application import application
from datetime import datetime
from core.utils import Tools
from unittest import TestCase
from tests import BaseTestClass
from flask_restplus import Namespace
from core.decorators import *
from flask import g
from flask_bcrypt import (generate_password_hash)
from core.database import Connector
from tests.v1.IntegrationTest.models import Test 


# Test functions core/decorators.py
class DecoratorsTestCase(BaseTestClass):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        apitest = Namespace('test', description='Apitest')
        cls.test_model = apitest.model('test_model', {
            'name': fields.String(required=True, description='Nombre que identifica la categoria.'),
            'description': fields.String(required=False, description='Descripción opcional.'),
            'popular': fields.Boolean(required=False, description='Bandera para categoría popular'),
        })
        cls.test_json = {
            "name":"test",
            "description":"test description",
            "popular":True
        }
        cls.fail_json = {
            "name":"",
            "description":"test description",
            "popular":True,
            "fail":"fail"
        }

        cls.headers1 = {
        'Content-Type': "application/json"
        }
        cls.headers2 = {
        'Content-Type': "application/json"
        }

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
    

# Test functions core/utils.py
class UtilsTestCase(BaseTestClass):
    @classmethod
    def setUpClass(cls):
        
        super().setUpClass()
        cls.username = "AlejandroRS1234"

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
    
    def test_response_maker_1(self):
        message="OK"
        code=200
        payload_result={"name":"nombre"}
        field_name = "campo"
        expected_return={
            "headerResponse": {
                "message": message,
                "code": code
                },
            "payload": payload_result
            }
        expected_return_w_field={
            "headerResponse": {
                "message": message,
                "code": code
                },
            "payload": {
                field_name: payload_result
                }
            }
        with application.app_context():
            result=Tools.response_maker_1(message,payload_result,code)
        json_data_success = result.get_json()
        print("json_data_success")
        print(json_data_success)
        self.assertDictEqual(json_data_success,expected_return)

        with application.app_context():
            result=Tools.response_maker_1(message,payload_result,code,field_name)
        json_data_success = result.get_json()
        print("json_data_success 2")
        print(json_data_success)
        self.assertDictEqual(json_data_success,expected_return_w_field)


# Test functions core/database.py
class DatabaseFunctionsTestCase(BaseTestClass):
    @classmethod
    def setUpClass(cls):
        # Global variables
        cls.conex = Connector()
        cls.collection_name = "test"

        cls.cat_test = {
            'name' : "CatTest",
            'description' : "Description CatTest",
            'deleted' : False,
            'created_by' : "1234",
            'created_at' : datetime.now(),
            'updated_by' : None,
            'updated_at' : None,
            'deleted_by' : None
        }
        cls.id_insert_test = str(Test.insert_one(cls.cat_test).inserted_id)
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.conex.close()
        super().tearDownClass()
    
    def test_edit_key_logic(self):
        """ Test method edit_key_logic"""
        json_success = {
            'id': self.id_insert_test,
            'description' : "Cualquier cosa"
        }
        json_not_found_error = {
            'id': "5f88cb009aa27262ea645be4",
            'description' : "Cualquier cosa"
        }
        json_invalid_id = {
            'id': "1234",
            'description' : "Cualquier cosa"
        }
        resultSuccess = self.conex.edit_key_logic(mongo_model = Test,
            collection_name = self.collection_name,
            json_data = json_success,
            user_id = "5f88cb009aa27262ea645be4",
            name_id = "id")
        resultNotFoundError = self.conex.edit_key_logic(mongo_model = Test,
            collection_name = self.collection_name,
            json_data = json_not_found_error,
            user_id = "5f88cb009aa27262ea645be4",
            name_id = "id")
        resultInvalidIdError = self.conex.edit_key_logic(mongo_model = Test,
            collection_name = self.collection_name,
            json_data = json_invalid_id,
            user_id = "5f88cb009aa27262ea645be4",
            name_id = "id")
        self.assertEqual(resultSuccess[1],200)
        self.assertEqual(resultNotFoundError[1],204)
        self.assertEqual(resultInvalidIdError[1],False)

