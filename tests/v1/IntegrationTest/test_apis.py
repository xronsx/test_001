# Flask libraries
# 3rd party libraries
# Standard/core python libraries
import unittest
from unittest import TestCase
# Our custom libraries
from application import application
from tests import BaseTestClass

# Write your apis tests here


class HomeApisTestCase(BaseTestClass):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Mount required to tests

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        # Unmount mounted to tests

    def test_example(self):
        '''
            Description test
        '''
