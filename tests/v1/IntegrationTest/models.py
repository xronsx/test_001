from pymongo import IndexModel
from pymongoext import *
from core.database import Connector

class Test(Model):
    __collection_name__ = "test"
    @classmethod
    def db(cls):
        conn = Connector()
        db = conn.db
        return db

    __schema__ = DictField(
        dict(
            name=StringField(required=True),
            description=StringField(required=False),
        ))

    __indexes__ = [IndexModel('name', unique=True)]
