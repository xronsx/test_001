from application import application
from dotenv import load_dotenv
from flask_cors import CORS
load_dotenv()

if __name__ == "__main__":
    CORS(application)
    application.run(debug=True, port=5000)
