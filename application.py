# -*- coding: utf-8 -*-
# Flask libraries
from flask import Flask
# 3rd party libraries
from flask_mail import Mail, Message
from flask_jwt_extended import JWTManager
from flask_cors import CORS, cross_origin
from werkzeug.middleware.proxy_fix import ProxyFix
# Standard/core python libraries
import datetime
import json
import os
# Our custom libraries
from apis.v1 import blueprint as v1
from core.database import Connector

# DEFAULT CONFIGURATION FLASK
application = Flask(__name__)
CORS(application)
application.wsgi_app = ProxyFix(application.wsgi_app, x_proto=1, x_host=1)
application.config['RESTPLUS_VALIDATE'] = False
#api.init_app(application)
application.register_blueprint(v1)
application.config['DEBUG'] = True

# DEFAULT JWT CONFIGURATION
application.config['JWT_SECRET_KEY'] = 'SECRET_KEY_PROJECT'
application.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)
application.config['JWT_BLACKLIST_ENABLED'] = True
application.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(application)
jwt._set_error_handler_callbacks(v1)

# DEFAULT EMAIL CONFIGURATION
mail = Mail(application)
application.config['MAIL_SERVER'] = 'smtpout.secureserver.net'
application.config['MAIL_PORT'] = 80
application.config['MAIL_USERNAME'] = 'dashbot@springlabs.net'
application.config['MAIL_PASSWORD'] = 't3mp0r4l'
application.config['MAIL_USE_TLS'] = True
application.config['MAIL_USE_SSL'] = False

# OPTIONAL CONFIGURATION
application.config['URL_BASE'] = "http://127.0.0.1:5000"
application.config['DEFAULT_SENDER'] = ("Springlabs", "hola@springlabs.net")
application.config['MAIL_CONTACT'] = "contact@springlabs.net"


# JWT BLACKLIST FUNCTION
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """
        False = No en lista negra
        True = En lista negra
    """
    jti = decrypted_token['jti']
    # Abro conexión
    conex = Connector()
    finding = conex.find_key('user_sessions', 'jti', jti, None, None)
    # Cierro conexión
    conex.close()
    if finding[1] != 200:
        return False
    else:
        if finding[0][0]['isDeleted'] != False:
            return True
    return False


if __name__ == "__main__":
    application.run(debug=True, port="5000")
