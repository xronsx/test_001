# -*- coding: utf-8 -*-
from .modelos.modelos import api as ns1
from .products import api as ns2
from flask import Blueprint
from flask_restplus import Api
import os
from dotenv import load_dotenv
load_dotenv()

authorizations = {
    'Bearer Auth': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': "Ingresa en el campo **'Value'** : **'Bearer &lt;JWT&gt;'**, donde JWT es el token de acceso",
    }
}
blueprint = Blueprint('api_v1', __name__, url_prefix='/v1')
api = Api(blueprint,
    title='Products Rubén APIS',
    version='1.0',
    description='Descripción de APIS Products Rubén <style>.models {display: none !important}</style>',
    authorizations=authorizations,
    security='Bearer Auth'
)
api.add_namespace(ns1)
api.add_namespace(ns2)
