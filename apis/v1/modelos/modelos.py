# -*- coding: utf-8 -*-

################################################################################
#                                                                              #
#                               CRUD - LOGIN                                   #
#                                                                              #
################################################################################

from flask import request, jsonify, make_response
from flask_restplus import Namespace, Resource, fields, marshal_with
from core.utils import Tools
from bson.json_util import dumps
import json
from flask import current_app
import os

api = Namespace('models', description='Manejo de modelos para documentación')


dataResponse = {
    "200": {
        "headerResponse": {
            "code": 200,
            "message": "OK"
        },
        "payload": {
            "client": "5dc1a8b14c95a955fd3a16fc",
            "email": "email@email.com",
            "lastName": "Herrera Lopez",
            "name": "José",
            "password": "pwd1234",
            "rol": "5dc1a8b14c95a955fd3a16fc",
            "username": "jose_herrera98"
        }
    },
    "409": {
        "headerResponse": {
            "code": 409,
            "message": "Ocurrio un error inesperado"
        },
        "payload": {}
    },
    "204_Duplicate": {
        "headerResponse": {
            "code": 204,
            "message": "El usuario ya existe en la base de datos"
        },
        "payload": {}
    },
    "204_NotFound": {
        "headerResponse": {
            "code": 204,
            "message": "ID de usuario no encontrado"
        },
        "payload": {}
    }
}

models_create = api.model('models_create', {
    'api': fields.String(required=True, example='user'),
    'description': fields.String(required=True, example='Manejo de usuarios'),
    'name': fields.String(required=True, example='insert'),
    'data': fields.Raw(example=dataResponse, type=json)

})

HEADER = "headerResponse"
PAYLOAD = "payload"


@api.route('/create', doc=False)
class Models(Resource):

    @api.expect(models_create)
    # @api.response(200, "Success", Agent_Platform_Agregar)
    def post(self):
        '''Crear modelos para documentación'''
        json_data = request.get_json(force=True)
        diccionario_respuesta = {}
        if os.path.exists("doc/" + json_data['api'] + "Response.py"):
            archivo = open("doc/" + json_data['api'] + "Response.py", "a+")
            contenido = archivo.read()
            final_de_archivo = archivo.tell()
        else:
            archivo = open("doc/" + json_data['api'] + "Response.py", "a+")
            contenido = archivo.read()
            final_de_archivo = archivo.tell()

            archivo.seek(final_de_archivo)
            base = "#-*- coding: utf-8 -*-\n"\
                "################################################################################\n"\
                "#                                                                              #\n"\
                "#                                RESPONSE - " + json_data['api'] + "                 \n"\
                "#                                                                              #\n"\
                "################################################################################\n"\
                "from flask_restplus import Namespace, Resource, fields, marshal_with\n"\
                "import json\n\n"\
                "api = Namespace('" + json_data['api'] + \
                "', description='" + json_data['description'] + "')"
            archivo.seek(final_de_archivo)
            archivo.write(base)
            archivo.seek(0)
            diccionario_respuesta['api'] = "api"
        if "200" in json_data['data'] and json_data['data']['200'] != {}:
            _200 = json_data['name'] + "_200"
            diccionario_respuesta['200'] = _200
            creacion_modelo = "\n\n" + json_data['name'] + "_200 = api.model('" + json_data['name'] + "_success_200_" + json_data['api'].upper() + "',{\n"\
                "'" + HEADER + "':  fields.Raw(example=" + str(json_data['data']['200'][HEADER]) + ", type=json),\n"\
                "'" + PAYLOAD + "':  fields.Raw(example=" + str(json_data['data']['200'][PAYLOAD]) + ", type=json),\n"\
                "})"
        else:
            creacion_modelo = "\n"
        archivo.seek(final_de_archivo)
        archivo.write(creacion_modelo)
        archivo.seek(0)
        nuevo_contenido = archivo.read()

        if "409" in json_data['data'] and json_data['data']['409'] != {}:
            _409 = json_data['name'] + "_409"
            diccionario_respuesta['409'] = _409
            creacion_modelo = "\n\n" + json_data['name'] + "_409 = api.model('" + json_data['name'] + "_Error_409_" + json_data['api'].upper() + "',{\n"\
                "'" + HEADER + "':  fields.Raw(example=" + str(json_data['data']['409'][HEADER]) + ", type=json),\n"\
                "'" + PAYLOAD + "':  fields.Raw(example=" + str(json_data['data']['409'][PAYLOAD]) + ", type=json),\n"\
                "})"
        else:
            creacion_modelo = "\n"
        archivo.seek(final_de_archivo)
        archivo.write(creacion_modelo)
        archivo.seek(0)
        nuevo_contenido = archivo.read()

        if "204_Duplicate" in json_data['data'] and json_data['data']['204_Duplicate'] != {}:
            _204_Duplicate = json_data['name'] + "_204_Duplicate"
            diccionario_respuesta['204_Duplicate'] = _204_Duplicate
            creacion_modelo = "\n\n" + json_data['name'] + "_204_Duplicate = api.model('" + json_data['name'] + "_Duplicate_204_" + json_data['api'].upper() + "',{\n"\
                "'" + HEADER + "':  fields.Raw(example=" + str(json_data['data']['204_Duplicate'][HEADER]) + ", type=json),\n"\
                "'" + PAYLOAD + "':  fields.Raw(example=" + str(json_data['data']['204_Duplicate'][PAYLOAD]) + ", type=json),\n"\
                "})"
        else:
            creacion_modelo = "\n"
        archivo.seek(final_de_archivo)
        archivo.write(creacion_modelo)
        archivo.seek(0)
        nuevo_contenido = archivo.read()

        if "204_NotFound" in json_data['data'] and json_data['data']['204_NotFound'] != {}:
            _204_NotFound = json_data['name'] + "_204_NotFound"
            diccionario_respuesta['204_NotFound'] = _204_NotFound
            creacion_modelo = "\n\n" + json_data['name'] + "_204_NotFound = api.model('" + json_data['name'] + "_NotFound_204_" + json_data['api'].upper() + "',{\n"\
                "'" + HEADER + "':  fields.Raw(example=" + str(json_data['data']['204_NotFound'][HEADER]) + ", type=json),\n"\
                "'" + PAYLOAD + "':  fields.Raw(example=" + str(json_data['data']['204_NotFound'][PAYLOAD]) + ", type=json),\n"\
                "})"
        else:
            creacion_modelo = "\n"
        archivo.seek(final_de_archivo)
        archivo.write(creacion_modelo)
        archivo.seek(0)
        nuevo_contenido = archivo.read()
        diccionario_respuesta = {
            "file": json_data['api'] + "Response.py",
            "imports": diccionario_respuesta
        }
        return diccionario_respuesta
