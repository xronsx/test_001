# -*- coding: utf-8 -*-

# Flask libraries
from flask import request
from flask_restplus import Namespace, Resource, fields
from flask import current_app
# 3rd party libraries
from flask_jwt_extended import (jwt_required)
# Standard/core python libraries
import json
import string
import bson
# Our custom libraries
from core.utils import Tools
from core.database import Connector
from core.decorators import *

from doc.productsResponse import (
    api,
    insert_200,
    insert_409,
    insert_204_Duplicate,
    get_200,
    get_409,
    get_204_NotFound,
    update_200,
    update_409,
    update_204_NotFound,
    delete_200,
    delete_409,
    delete_204_NotFound,

)

from core.models import Products as mongo_model

add_product = api.model('add_product', {
    'name': fields.String(required=True,
        description='Nombre del producto',
        example="Producto 1"),
    'category': fields.String(required=True,
        description='Categoría del producto',
        example="Categoría 1"),
    'description': fields.String(required=True,
        description='Descripción del producto',
        example="Descripción de producto 1"),
    'price': fields.String(required=True,
        description='Precio del producto',
        example=1266.50),
    'quantity': fields.Integer(required=True,
        description='Cantidad de productos',
        example=10),
    'inventory': fields.String(required=True,
        description='Inventario del producto',
        example="En Stock, Agotado, Limitado"),
    'image': fields.String(required=True,
        description='Imagen del producto',
        example="imagenbase64"),
    }
)

update_product = api.model('update_product', {
    'id': fields.String(required=True,
        description='id del producto que se requiere editar',
        example="6009c97fcbb6a7cb0ecac3bf"),
    'name': fields.String(required=False,
        description='Nombre del producto',
        example="Producto 1"),
    'category': fields.String(required=False,
        description='Categoría del producto',
        example="Categoría 1"),
    'description': fields.String(required=False,
        description='Descripción del producto',
        example="Descripción de producto 1"),
    'price': fields.String(required=False,
        description='Precio del producto',
        example=1266.50),
    'quantity': fields.Integer(required=False,
        description='Cantidad de productos',
        example=10),
    'inventory': fields.String(required=False,
        description='Inventario del producto',
        example="En Stock, Agotado, Limitado"),
    'image': fields.String(required=False,
        description='Imagen del producto',
        example="imagenbase64"),
    }
)

delete_product = api.model('delete_product', {
    'id': fields.String(required=True,
        description='id del producto que se va a eliminar',
        example='6009c97fcbb6a7cb0ecac3bf')
})

ALLOWED_INVENTORY = ['En Stock', 'Agotado', "Limitado"]

@api.route('')
class Producto(Resource):
    @api.expect(add_product)
    @api.response(200, "Success", insert_200)
    @api.response(204, "Producto duplicado", delete_204_NotFound)
    @api.response(409, "Error inesperado", insert_409)
    def post(self):
        '''Agregar producto'''
        json_data = request.get_json(force=True)
        id_user = "5e162190647d0f2b0e2867bd"
        if "inventory" in json_data:
            if json_data['inventory'] not in ALLOWED_INVENTORY:
                result = Tools.response_maker_1("Tipo de inventario no valido", {}, 204, None)
                return result

        conex = Connector()
        result = conex.insert_one_logic(mongo_model = mongo_model,
            collection_name = mongo_model.__collection_name__,
            data = json_data,
            id = id_user)
        if result[1] == 200:
            result = result[0]
            message = "Se agregó producto correctamente"
            result = Tools.response_maker_1(message, result, 200, None)
        elif result[1] == 204:
            result = {}
            result = Tools.response_maker_1("Nombre de producto duplicado", {}, 204, None)
        else:
            data = {}
            mensaje = result[0]['message']
            result = Tools.response_maker_1(
                mensaje, data, result[0]['code'], None)
        # Cierro conexión
        conex.close()

        return result

    
    @api.doc(params={'id': 'Obtener un solo producto por id'})
    @api.response(200, "Success", get_200)
    @api.response(204, "Producto no encontrado", get_204_NotFound)
    @api.response(409, "Error inesperado", get_409)
    def get(self):
        '''Obtener todos los productos'''
        filters = {}
        if request.args.get('id') != None:
            if bson.objectid.ObjectId.is_valid(request.args.get('id')):
                filters = {'_id': ObjectId(request.args.get('id'))}
            else:
                data = {}
                mensaje = "El id de producto (" + request.args.get(
                    'id') + ") no es un id valido "
                result = Tools.response_maker_1(mensaje, data, 204, None)
                return result

        # Abro conexion
        conex = Connector()
        result = conex.find_key_dynamic_logic(collection_name=mongo_model.__collection_name__,
            filter_mongo=filters)

        conex.close()
        if result[1] == 200:
            result = Tools.response_maker_1("OK", result[0], 200, "products")

        else:
            data = {}
            mensaje = result[0]['message']
            result = Tools.response_maker_1(
                mensaje, data, result[0]['code'], None)

        return result

    @api.expect(delete_product)
    @api.response(200, "Success", delete_200)
    @api.response(204, "Producto duplicado", insert_204_Duplicate)
    @api.response(409, "Error inesperado", delete_409)
    def delete(self):
        '''Eliminar Producto (Mandando id por body)'''
        json_data = request.get_json(force=True)
        id_user = "5e162190647d0f2b0e2867bd"
        conex = Connector()
        result = conex.delete_logic(collection_name=mongo_model.__collection_name__,
            json_data=json_data,
            user_id=id_user,
            unique_fields=["name"])
        if result[1] == 200:
            result = {}
            message = "Se eliminó el producto correctamente"
            result = Tools.response_maker_1(message, result, 200, None)
        elif result[1] == 204:
            result = {}
            result = Tools.response_maker_1(
                "id de producto no encontrado", {}, 204, None)
        else:
            data = {}
            mensaje = result[0]['message']
            result = Tools.response_maker_1(
                mensaje, data, result[0]['code'], None)
        # Cierro conexión
        conex.close()

        return result

    @api.expect(update_product)
    @valid_values_empty(['id'])
    @api.response(200, "Success", update_200)
    @api.response(204, "Producto no encontrado", update_204_NotFound)
    @api.response(409, "Error inesperado", update_409)
    def put(self):
        '''Editar Producto (Mandando id por body)'''
        json_data = request.get_json(force=True)
        id_user="5e162190647d0f2b0e2867bd"

        if "inventory" in json_data:
            if json_data['inventory'] not in ALLOWED_INVENTORY:
                result = Tools.response_maker_1("Tipo de inventario no valido", {}, 204, None)
                return result

        conex = Connector()
        result = conex.edit_key_logic(mongo_model=mongo_model,
            collection_name=mongo_model.__collection_name__,
            json_data=json_data,
            user_id=id_user,
            name_id="id")
        if result[1] == 200:
            data = result[0]
            message = "Se modificó el producto correctamente"
            result = Tools.response_maker_1(message, data, 200, None)
        elif result[1] == 204:
            result = {}
            result = Tools.response_maker_1(
                "id de producto no encontrado", {}, 204, None)
        else:
            data = {}
            if result[0]['message'] == "DuplicateKeyError":
                mensaje = "El producto con nombre " + \
                    json_data['name'] + " ya existe en la DB"
            else:
                mensaje = result[0]['message']
            result = Tools.response_maker_1(
                mensaje, data, result[0]['code'], None)
        # Cierro conexión
        conex.close()

        return result

