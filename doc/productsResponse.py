#-*- coding: utf-8 -*-
################################################################################
#                                                                              #
#                                RESPONSE - products                 
#                                                                              #
################################################################################
from flask_restplus import Namespace, Resource, fields, marshal_with
import json

api = Namespace('products', description='Manejo de productos')

insert_200 = api.model('insert_success_200_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 200, 'message': 'Se agregó producto correctamente'}, type=json),
'payload':  fields.Raw(example={'_id': {'$oid': '6009f4e9a0c39755d9645f94'}, 'category': 'Categoría 1', 'description': 'Descripción de producto 1', 'image': 'imagenbase64', 'inventory': 'En Stock', 'name': 'Producto 3', 'price': 1266.5, 'quantity': 10}, type=json),
})

insert_409 = api.model('insert_Error_409_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 409, 'message': 'Ocurrio un error inesperado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})

insert_204_Duplicate = api.model('insert_Duplicate_204_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 204, 'message': 'Nombre de producto duplicado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})


get_200 = api.model('get_success_200_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 200, 'message': 'OK'}, type=json),
'payload':  fields.Raw(example={'products': [{'_id': {'$oid': '6009f3f3fe1a100d20b6aecc'}, 'category': 'Categoría 1', 'description': 'Descripción de producto 1', 'image': 'imagenbase64', 'inventory': 'En Stock', 'name': 'Producto 3', 'price': 1266.5, 'quantity': 10}]}, type=json),
})

get_409 = api.model('get_Error_409_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 409, 'message': 'Ocurrio un error inesperado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})


get_204_NotFound = api.model('get_NotFound_204_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 204, 'message': 'El id de producto (12345) no es un id valido'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})

update_200 = api.model('update_success_200_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 200, 'message': 'Se modificó el producto correctamente'}, type=json),
'payload':  fields.Raw(example={'_id': {'$oid': '6009f4e9a0c39755d9645f94'}, 'category': 'Categoría 2', 'description': 'Descripción de producto 1', 'image': 'imagenbase64', 'inventory': 'En Stock', 'name': 'Producto 3', 'price': 1266.5, 'quantity': 10}, type=json),
})

update_409 = api.model('update_Error_409_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 409, 'message': 'Ocurrio un error inesperado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})


update_204_NotFound = api.model('update_NotFound_204_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 204, 'message': 'id de producto no encontrado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})

delete_200 = api.model('delete_success_200_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 200, 'message': 'Se eliminó el producto correctamente'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})

delete_409 = api.model('delete_Error_409_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 409, 'message': 'Ocurrio un error inesperado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})


delete_204_NotFound = api.model('delete_NotFound_204_PRODUCTS',{
'headerResponse':  fields.Raw(example={'code': 204, 'message': 'id de producto no encontrado'}, type=json),
'payload':  fields.Raw(example={}, type=json),
})