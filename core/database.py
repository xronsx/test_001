# -*- coding: utf-8 -*-
# Flask libraries
# 3rd party libraries
from flask_bcrypt import (check_password_hash)

from pymongo import MongoClient, errors
from bson.json_util import dumps
from bson.objectid import ObjectId
from bson.errors import *
# Standard/core python libraries
from datetime import datetime
import json
import os
# Our custom libraries
from core.utils import Tools
from flask import current_app


class Connector:
    def __init__(self):
        """Constructor de conexión a MongoDB
        """
        mongoClient, dbtype = Tools.database_connection()
        if dbtype == "atlas":
            self.db = mongoClient.get_database(
                '' + os.environ.get('MONGO_DATABASE', "default"))
        elif dbtype == "localhost":
            self.db = getattr(mongoClient, os.environ.get(
                'MONGO_DATABASE', "default"))
        else:
            self.db = mongoClient.default
        self.mongoClient = mongoClient
    
    def close(self):
        """Cierra conexión de objeto MongoDB
        """
        self.mongoClient.close()


    def insert_one(self, collection_name, json_data):
        """Inserta un registro a la base de datos

        Args:
            collection_name (str): Nombre de colección a donde se hará inserción
            json_data (dict): Diccionario a insertar en MongoDB

        Returns:
            [tuple]: Result (Dict) , 200|204|False
        
        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            CollectionInvalid: Si la validación de la colección falla
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            WriteError: Si ocurre un problema de escritura en DB
        """
        try:
            collection = self.db[collection_name]
            insertion = collection.insert_one(json_data)
            message='Inserción en colección '+collection_name
            result = insertion, 200
        except errors.CursorNotFound as e:
            message =str(e)
            return {"message": "CursorNotFound", "code": 409}, False
        except errors.CollectionInvalid as e:
            message = str(e)
            result = {"message": "CollectionInvalid", "code": 409}, False
        except errors.DuplicateKeyError as e:
            message = str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, 204
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 409}, False
        except Exception as e:
            message  =str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        return result

    def find_key_dynamic_logic(self, collection_name, filter_mongo, exclude_mongo={}, field_order=None, type_order=None):
        """Función que busca y retorna uno o varios registros lógicos

        Args:
            collection_name (str): Colección en la que se buscara el registro
            filter_mongo (dict): Valores que se buscarán en la colección (filtro)
            exclude_mongo (dict): Valores que se excluirán de la consulta (exclude)
            field_order (dict): Campo por el que se ordenará la consulta
            type_order (dict): Tipo por el que se ordenará la consulta (ASC|DESC)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            InvalidId: Id invalido para formato BSON
        """
        try:
            key_json = {}
            collection = self.db[collection_name]
            exclude_mongo['isDeleted'] = 0
            exclude_mongo['createdOn'] = 0
            exclude_mongo['createdBy'] = 0
            exclude_mongo['modifiedOn'] = 0
            exclude_mongo['modifiedBy'] = 0
            filter_mongo['isDeleted'] = False
            if field_order != None:
                if type_order == "ASC":
                    type_order = 1
                elif type_order == "DESC":
                    type_order = -1
                key = collection.find(filter_mongo, exclude_mongo).sort(
                    field_order, type_order)
            else:
                key = collection.find(filter_mongo, exclude_mongo)
            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
                result = key_json, 200
                message = "Busqueda en colección "+collection_name
            else:
                message = "Id no encontrado en colección "+collection_name
                result = {"message": "Id no encontrado", "code": 204}, 204
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        return result

    def delete_key(self, collection_name, field, value):
        """Función que elimina un registro físicamente de la base de datos

        Args:
            collection_name (str): Colección en la que se eliminará el registro
            field (str): Campo en el que se buscará el registro
            value (str): Valor del campo con el que se buscara el registro
        
        Returns:
            [tuple]: Result (Dict) , True|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
        """
        try:
            key_json = {}
            collection = self.db[collection_name]
            if field != "_id":
                key = collection.remove({field: str(value)})
            else:
                key = collection.remove({field: ObjectId(value)})
            key_dumps = dumps(key)
            key_json = json.loads(key_dumps)
            if key_json['n'] > 0:
                result = key_json, 200
                message = "Eliminación en colección "+collection_name+", "+field+" "+value
            else:
                message = "Falló eliminación en colección "+collection_name+", "+field+" "+value
                result = key_json, False
        except errors.CursorNotFound as e:
            message = str(e)
            result = str(e.details), False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def insert_one_logic(self, mongo_model, collection_name, data, id, exclude_mongo={}):
        """Función que inserta un elemento logicamente a una colección dada

        Función que inserta un registro en una colección especifica, se agregan
        los datos lógicos (isDeleted, createdOn, createdBy, modifiedOn, modifiedBy)

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            collection_name (str): Colección en la que se buscarán los registros
            data (dict): Datos en formato JSON que se insertarán
            id (str): Id del usuario que ejecutó la inserción
            exclude_mongo (dict): Valores que se excluirán de la consulta (exclude)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            CollectionInvalid: Si la validación de la colección falla
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            WriteError: Si ocurre un problema de escritura en DB
        """
        created_date=datetime.now()
        data['isDeleted'] = False
        data['createdBy'] = id
        data['createdOn'] = created_date
        data['modifiedBy'] = None
        data['modifiedOn'] = None
        exclude_mongo['isDeleted'] = 0
        exclude_mongo['createdOn'] = 0
        exclude_mongo['createdBy'] = 0
        exclude_mongo['modifiedOn'] = 0
        exclude_mongo['modifiedBy'] = 0
        try:
            collection = self.db[collection_name]
            id_insert = mongo_model.insert_one(data).inserted_id
            key = collection.find({"_id": ObjectId(id_insert)},exclude_mongo)
            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
            message='Inserción en colección '+collection_name+", id "+str(id_insert)
            result = key_json[0],200
        except errors.CursorNotFound as e:
            message=str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except errors.CollectionInvalid as e:
            message=str(e)
            result = {"message": "CollectionInvalid", "code": 409}, False
        except errors.DuplicateKeyError as e:
            message=str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, 204
        except errors.WriteError as e:
            message=str(e)
            result = {"message": "WriteError", "code": 409}, False
        except Exception as e:
            message=str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False

        
        return result
    
    def listar_simple_logic(self, collection_name):
        """Función que obtiene todos los elementos de una colección

        Función que obtiene todos los elementos lógicos de una colección especifica,
        se omiten los datos lógicos (isDeleted, createdOn, createdBy,modifiedOn, modifiedBy)

        Args:
            collection_name (str): Colección en la que se buscarán los registros
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
        """
        try:
            collection = self.db[collection_name]
            exclude = {
                "isDeleted": 0,
                "createdOn": 0,
                "createdBy": 0,
                "modifiedOn": 0,
                "modifiedBy": 0
            }
            key = collection.find({"isDeleted": False}, exclude)
            key_dumps = dumps(key)
            key_json = json.loads(key_dumps)
            message ="Busqueda en colección "+collection_name
            result = key_json, 200
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def delete_logic(self, collection_name, json_data, user_id, unique_fields=[]):
        """Función que elimina un elemento logicamente de una colección dada

        Función que elimina logicaménte un registro en una colección especifica,
        se modifican los datos lógicos (isDeleted, modifiedOn, modifiedBy)

        Args:
            collection_name (str): Colección en la que se eliminarán los registros
            json_data (dict): Datos en formato JSON que contiene el id a eliminar
            user_id (str): Id del usuario que ejecutó la acción
            unique_fields (list): Lista de parametros unicos de la colección
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            InvalidId: Id invalido para formato BSON
        """
        try:
            collection = self.db[collection_name]
            k = json_data.keys()
            key = list(k)
            id_reg = json_data[key[0]]
            datos = {"isDeleted": True, "modifiedBy": user_id,"modifiedOn": datetime.now()}
            myquery = {"_id": ObjectId(json_data[key[0]]), "isDeleted": False}
            key = collection.find(myquery)
            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
                datos = {
                    "isDeleted": True,
                    "modifiedBy": user_id,
                    "modifiedOn": datetime.now()
                }
                count = 0
                for items in unique_fields:
                    nombre = key_json[0][items]
                    datos[items] = nombre + ".baja." + id_reg
                newvalues = {"$set": datos}
                borrado = collection.update_one(myquery, newvalues)
                message = "Eliminación en colección "+collection_name+", id "+id_reg
                result = "Ok", 200
            else:
                message = "Falló eliminación en colección "+collection_name+", id "+id_reg
                result = {"message": "Id no encontrado", "code": 204}, 204
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def edit_key_logic(self, mongo_model, collection_name, json_data, user_id, name_id, exclude_mongo={}):
        """Función que edita un elemento de una colección dada

        Función que edita un registro en una colección especifica,
        se modifican los datos lógicos (modifiedOn, modifiedBy)

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la modificación
            collection_name (str): Colección en la que se buscarán los registros
            json_data (dict): Datos en formato JSON que contiene los datos nuevos
            user_id (str): Id del usuario que ejecutó la acción
            name_id (str): Nombre del key donde se manda ID a editar
            exclude_mongo (dict): Valores que se excluirán de la consulta (exclude)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            InvalidId: Id invalido para formato BSON
        """
        exclude_mongo['isDeleted'] = 0
        exclude_mongo['createdOn'] = 0
        exclude_mongo['createdBy'] = 0
        exclude_mongo['modifiedOn'] = 0
        exclude_mongo['modifiedBy'] = 0
        mod = {"modifiedOn": datetime.now()}
        if user_id!="N/A" and user_id!=None and user_id!="":
            mod["modifiedBy"]=user_id
        if "publish_date" in json_data:
            if json_data['publish_date'] == True:
                mod['publish_date'] = datetime.now().timestamp()
                json_data.pop('publish_date', None)
        try:
            collection = self.db[collection_name]
            myquery = {"_id": ObjectId(json_data[name_id])}
            key = self.db[collection_name].find(myquery)
            if key.count() > 0:
                origin = json_data
                json_data = json.dumps(json_data)
                json_data = json.loads(json_data)
                datos = []
                datos.append(mod)
                keys = origin.keys()
                for dato in keys:
                    if dato != name_id:
                        datos[0][dato] = json_data[dato]
                newvalues = {"$set": datos[0]}
                borrado = mongo_model.update_one(myquery, newvalues)
                key_find = collection.find(myquery,exclude_mongo)
                if key_find.count() > 0:
                    key_dumps = dumps(key_find)
                    key_json = json.loads(key_dumps)
                result = key_json[0],200
                message = "Actualización en colección "+collection_name+", id "+json_data[name_id] 
            else:
                result = {"message": "Id no encontrado", "code": 204}, 204
                message = "Id no encontrado en colección "+collection_name+", id "+json_data[name_id] 
        except errors.DuplicateKeyError as e:
            message = str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            print(message)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def authenticate(self, collection_name, json_data):
        """Función que autentica el inicio de sesión de un usuario

        Función que busca un usuario dado en la base de datos y valida
        las contraseñas para iniciar sesión

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            json_data (dict): Datos en formato JSON que contiene los datos de autenticación
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
        """
        try:
            key_json = {}
            collection = self.db[collection_name]
            key = collection.find({"id_employee": json_data['id_employee']})
            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
                key_base = key_json[0]['password']
                if check_password_hash(key_base, json_data['password']):
                    result = key_json[0], 200
                    message = "Autenticación id_employee "+json_data['id_employee']
                else:
                    result = 0, 204
                    message = "Contraseña incorrecta, id de empleado "+json_data['id_employee']
            else:
                result = 0, 204
                message = "Falló autenticación id de empleado "+json_data['id_employee']
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, 409
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, 409
        
        
        return result

    def exist_keys_logic(self, collection_name, json_data):
        """Función que comprueba si elemento esta borrado lógicamente

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            json_data (dict): JSON con los campos y valores para realizar la busqueda
        
        Returns:
            [tuple]: Result (Dict) , 0|1|2|False
                0: No existe elemento en DB (Insertar)
                1: Existe elemento en DB perp eliminado logicaménte (Editar)
                2: Existe elemento activo en DB (No se puede insertar)
                False: Error inesperado
        """
        try:
            collection = self.db[collection_name]
            exclude = [
                "isDeleted",
                "createdOn",
                "createdBy",
                "modifiedOn",
                "modifiedBy",
            ]

            document = collection.find_one(json_data)
            if document == None:
                key_json = {} 
                result = key_json, 0
                message = "No existente, colección "+collection_name
            elif document['isDeleted'] == True:
                for (key, val) in list(document.items()):
                    if key in exclude:
                        del document[key]
                key_dumps = dumps(document)
                key_json = json.loads(key_dumps)
                message = "No eliminado lógicamente, colección "+collection_name             
                result = key_json, 1
            elif document['isDeleted'] == False:
                for (key, val) in list(document.items()):
                    if key in exclude:
                        del document[key]
                key_dumps = dumps(document)
                key_json = json.loads(key_dumps)
                message = "Eliminado lógicamente, colección "+collection_name
                result = key_json, 2
            else:
                message = "Ocurrio un error inesperado"
                result = {"message": "Ocurrio un error inesperado",
                    "code": 409}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def find_unique_keys(self, unique_list):
        """Función encargada de buscar keys únicas

        Args:
            unique_list (list): Lista de objetos llaves únicas a buscar
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False
        
        """
        try:
            exists_list = []
            for unique_object in unique_list:
                query = self.find_key_dynamic_logic(
                    unique_object['nombre_coleccion'], {unique_object['parametro']: unique_object['item']})
                if query[1] == 200:
                    id_c = query[0][0]['_id']['$oid']
                    if unique_object['id_ex'] != id_c:
                        mensaje = "El " + unique_object['parametro'] + \
                            " " + unique_object['item'] + " ya existe en la DB"
                        exists_list.append(mensaje)
            if len(exists_list) > 0:
                message = exists_list[0] 
                result = exists_list, 204
            else:
                message = "Parametros no existentes"
                result = "ok", 200
        except:
            message = "Ocurrio un error inesperado"
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        return result

    def edit_array_key_logic(self, mongo_model, collection_name, name_key, values, user_id, object_id):
        """Actualiza los datos de un arreglo de un documento

        Función que actualiza los datos de un arreglo de un documento
        y actualiza los datos de modificación

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            collection_name (str): Colección en la que se modificara
            name_key (dict): Clave en la cual se insertarán los datos
            values (dict): Valores que se insertarán al documento
            user_id (str): Id del usuario que ejecutó la acción
            object_id (str): Id del documento que va a ser modificado
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            WriteError: Si ocurre un problema de escritura en DB
        """
        mod = {"modifiedOn": datetime.now()}
        if user_id!="N/A" and user_id!=None and user_id!="":
            mod["modifiedBy"]=user_id
        findId = {"_id": ObjectId(object_id)}
        updateQuery = {"$addToSet": {name_key: values}}
        updateQuery2 = {"$set": mod}
        try:
            update = mongo_model.update_one(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                result = {"message": "No hubo modificaciones", "code": 204}, 204
                message = "No hubo modificaciones en colección "+collection_name+", id "+object_id
            else:
                update = mongo_model.update_one(findId, updateQuery2)
                result = "Ok", 200
                message = "Modificaciones en arreglos, colección "+collection_name+", id "+object_id
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def find_in_array(self, collection_name, field, json_data, json_parameter, json_parameter_array):
        """Busca un elemento en un arreglo

        Args:
            collection_name (str): Colección en la que se modificara
            field (str): Campo de busqueda
            json_data (dict): Diccionario donde se encuentra data a buscar
            json_parameter (str): Key del parámetro a buscar en JSON
            json_parameter_array (str): Key del elemento del array que se desea buscar en arreglo
        
        Returns:
            [tuple]: Result (Dict) , 200
        """
        for obj in json_data[json_parameter]:
            result = self.find_key_dynamic_logic(
                collection_name, {field: ObjectId(obj[json_parameter_array])})
            if result[1] == 200:
                obj[json_parameter_array] = result[0][0]
        
        
        

        return json_data, 200

    def delete_from_array(self, mongo_model, name_key, value, object_id, user_id):
        """Elimina un elemento de un arreglo

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la eliminación
            name_key (dict): Clave en la cual se eliminarán los datos
            value (str): Valor que se eliminará del documento
            elemento (str): Id del documento que va a ser modificado
            user_id (str): Id del usuario que ejecutó la acción
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False
        """
        mod = {"modifiedBy": user_id, "modifiedOn": datetime.now()}
        findId = {"_id": ObjectId(object_id)}
        updateQuery = {"$pull": {name_key: value}}
        updateQuery2 = {"$set": mod}
        try:
            update = mongo_model.update_one(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                result = {"message": "No hubo modificaciones", "code": 204}, 204
                message = "No hubo modificaciones en colección "+mongo_model.__collection_name__
            else:
                update = mongo_model.update_one(findId, updateQuery2)
                message = "Eliminación en arreglos, colección "+mongo_model.__collection_name__
                result = "Ok", 200
            
        except Exception as e:
            message=str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
       
        return result

    def find_key_by_date(self, collection_name, field, value, date_field, start_date, end_date, distinct=None):
        """Función que busca y retorna registros ordenados por fecha

        Función que busca en una colección por medio de un campo y rangos de fecha,
        existe el parámetro opcional "distinct", si se ocupa se hará una distinción
        por el valor que se mande

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            field (str): Campo por el que se buscara el registro
            value (str): Valor del campo por el que se buscará el registro
            date_field (str): Campo por el que se filtrarán las fechas
            start_date (str): Fecha de inicio de rango de fecha
            end_date (str): Fecha de inicio de rango de fecha
            distinct (str): Campo por el que se quiere hacer una distinción
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            InvalidId: ID invalido en formato BSON
        """
        try:
            key_json = {}
            collection = self.db[collection_name]

            if distinct != None:
                if field != "_id":
                    key = collection.find({field: str(value), date_field: {"$gte": str(
                        start_date), "$lte": str(end_date)}}).distinct(distinct)
                else:
                    key = collection.find({field: ObjectId(value), date_field: {
                        "$gte": str(start_date), "$lte": str(end_date)}}).distinct(distinct)
            else:
                if field != "_id":
                    key = collection.find({field: str(value), date_field: {
                        "$gte": str(start_date), "$lte": str(end_date)}})
                else:
                    key = collection.find({field: ObjectId(value), date_field: {
                        "$gte": str(start_date), "$lte": str(end_date)}})
                if key.count() > 0:
                    key_dumps = dumps(key)
                    key_json = json.loads(key_dumps)
                    result = key_json, 200
                else:
                    key_json = []
                    result = key_json, 200
                message ="Busqueda en colección "+collection_name
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def aggregation(self, collection_name, pipeline):
        """Función que hace aggregation en mongo a una colección dada
        
        Args:
            collection_name (str): Colección en la que se buscarán los registros
            pipeline (dict): Pipeline de aggregation mongo
        
        Returns:
            [tuple]: Result (Dict) , 200|False
        """
        try:
            collection = self.db[collection_name]
            result_a = collection.aggregate(pipeline)
            result = list(result_a), 200
            message = "aggregation en colección" + collection_name
        except Exception as e:
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
            message = "Ocurrio un error inesperado"
        
        
        return result

    def edit_only_keys_logic(self, mongo_model, object_id, new_values, user_id):
        """ Actualiza los valores de un documento

        Función que actualiza los valores de un documento y actualiza los datos de modificacion

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            object_id (str): Id de elemento a modificar
            new_values (dict): Nuevos valores del documento
            user_id (str): Id del usuario que ejecutó la acción
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            WriteError: Si ocurre un problema de escritura en DB
        """
        if str(type(mongo_model)) == "<class 'str'>":
            mongo_model = self.db[mongo_model]
        else:
            mongo_model = mongo_model
        try:
            mod = {'modifiedBy': user_id, 'modifiedOn': datetime.now()}
            mod.update(new_values)
            findId = {'_id': ObjectId(object_id)}
            updateQuery = {'$set': mod}
            update = mongo_model.update_one(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                result = {"message": "No hubo modificaciones", "code": 204}, 204
                message = "No hubo modificaciones en colección "+mongo_model.__collection_name__
            else:
                result = "Ok", 200
                message = "Actualización en colección "+mongo_model.__collection_name__
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False

        
        return result

    def get_all(self, collection_name, foreign_list=[], filter_mongo={}, exclude_mongo={}):
        """Función que obtiene todos o solo un elemento de una colección

        Función que obtiene todos los elementos lógicos de una colección especi-
        fica, se omiten los datos lógicos (isDeleted, createdOn, createdBy,
        modifiedOn, modifiedBy). El parametro elemento de tener el valor "None"
        La funcion también puede obtener un solo elemento lógico de la colección.
        El parametro elemento debe contener el id del elemento buscado en la base
        de datos

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            foreign_list (list): Arreglo de objetos con la información de los datos
                foraneos de los elementos de la colección
            filter_mongo (dict): Valores para consulta de información (filter)
            exclude_mongo (dict): Valores que se excluirán de la consulta (exclude)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False
        """
        resultado = self.find_key_dynamic_logic(collection_name, filter_mongo, exclude_mongo = exclude_mongo)
        if resultado[1] != 200:
            return resultado
        else:
            json_data = resultado[0]
            for r in json_data:
                for item in foreign_list:
                    if item['tipo'] == "unico":
                        if r[item['campo']] != "":
                            consulta = self.find_key_dynamic_logic(
                                item['coleccion'], {"_id": ObjectId(r[item['campo']])},
                                exclude_mongo = exclude_mongo)
                        else:
                            consulta = "", 204
                            r[item['campo']] = ""
                        if consulta[1] == 200:
                            new_value = consulta[0][0]
                            r[item['campo']] = new_value
                        else:
                            r[item['campo']] = ""
                    elif item['tipo'] == "lista":
                        arreglo_in = []
                        prueba = r[item['campo']]
                        for valor in r[item['campo']]:
                            consulta = self.find_key_dynamic_logic(
                                item['coleccion'], {"_id": ObjectId(valor)},
                                exclude_mongo = exclude_mongo)
                            if consulta[1] == 200:
                                new_value = consulta[0][0]
                                arreglo_in.append(new_value)
                        r[item['campo']] = arreglo_in
                    elif item['tipo'] == "nested":
                        nestedField = item['campo'].split('.')
                        if not nestedField[0] in r or not nestedField[1] in r[nestedField[0]]:
                            continue
                        consulta = self.find_key_dynamic_logic(
                            item['coleccion'], {"_id": ObjectId(r[nestedField[0]][nestedField[1]])},exclude_mongo = exclude_mongo)
                        if consulta[1] == 200:
                            r[nestedField[0]][nestedField[1]] = consulta[0][0]
                    elif item['tipo'] == "nested-list":
                        resultList = []
                        nestedField = item['campo'].split('.')
                        for valor in r[nestedField[0]][nestedField[1]]:
                            consulta = self.find_key_dynamic_logic(
                                item['coleccion'], {"_id": ObjectId(valor)},
                                exclude_mongo = exclude_mongo)
                            if consulta[1] == 200:
                                resultList.append(consulta[0][0])
                        r[nestedField[0]][nestedField[1]] = resultList
            
            return json_data, 200

    def update_many_array_logic(self, mongo_model, collection_name, name_key, values, user_id, filter_mongo):
        """Actualiza los datos de un arreglo de un documento

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            collection_name (str): Colección en la que se buscarán los registros
            name_key (str): Clave en la cual se insertarán los datos
            values (dict): Valores que se insertarán al documento
            user_id (str): Id del usuario que ejecutó la acción
            filter_mongo (dict): Valores para realizar la consulta (filter)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            WriteError: Si ocurre un problema de escritura en DB
        """
        mod = {"modifiedBy": user_id, "modifiedOn": datetime.now()}
        findId = filter_mongo
        updateQuery = {"$addToSet": {name_key: values}}
        updateQuery2 = {"$set": mod}
        try:
            update = mongo_model.update_many(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                result = {"message": "No hubo modificaciones", "code": 204}, 204
                message = "No hubo modificaciones en colección "+collection_name
            else:
                update = mongo_model.update_many(findId, updateQuery2)
                result = "Ok", 200
                message = "Actualización en colección "+collection_name
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def delete_many_from_array(self, mongo_model, name_key, value, filter_mongo, user_id):
        """Elimina varios elementos de un arreglo

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la eliminación
            name_key (str): Clave en la cual se eliminarán los datos
            value (str): Valor de la clae en la cual se eliminarán los datos
            filter_mongo (dict): Valores para realizar la consulta (filter)
            user_id (str): Id del usuario que ejecutó la acción
            
        Returns:
            [tuple]: Result (Dict) , 200|204|False
        """
        mod = {"modifiedBy": user_id, "modifiedOn": datetime.now()}
        findId = filter_mongo
        updateQuery = {"$pull": {name_key: value}}
        updateQuery2 = {"$set": mod}
        try:
            update = mongo_model.update_many(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                result = {"message": "No hubo modificaciones", "code": 204}, 204
                message = "Eliminación en arreglos, colección "+mongo_model.__collection_name__
            else:
                message = "No hubo modificaciones en colección "+mongo_model.__collection_name__
                update = mongo_model.update_many(findId, updateQuery2)
                result = "Ok", 200

        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def edit_only_keys(self, mongo_model, object_id, new_values, user_id):
        """Actualiza los valores de un documento

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la modificación
            object_id (str): Id del documento que se va a actualizar
            new_values (dict): JSON con los campos y valores que se van a actualizar
            user_id (str): Id del usuario que ejecutó la inserción
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            WriteError: Si ocurre un problema de escritura en DB
        """
        if str(type(mongo_model)) == "<class 'str'>":
            mongo_model = self.db[mongo_model]
        else:
            mongo_model = mongo_model
        try:
            mod = new_values
            findId = {'_id': ObjectId(object_id)}
            updateQuery = {'$set': mod}
            update = mongo_model.update_one(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                message = "No hubo modificaciones en colección "+mongo_model.__collection_name__
                result = {"message": "No hubo modificaciones", "code": 204}, 204
            else:
                message = "Actualización en colección "+mongo_model.__collection_name__
                result = "Ok", 200
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        return result

    def find_key(self, collection_name, field, value, order_field=None, order_type=None):
        """Función que busca y retorna un registro

        Función que busca en una colección por medio de un campo y retorna el resultado

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            field (str): Campo por el que se buscara el registro
            value (str): Valor que se buscará en el registro
            order_field (str): Campo por que el que se ordenará la consulta
            order_type (dict): Tipo por el que se ordenará la consulta (ASC|DESC)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            InvalidId: Id invalido para formato BSON
        """
        try:
            key_json = {}
            collection = self.db[collection_name]

            if order_field != None:
                if order_type == "ASC":
                    order_type = 1
                elif order_type == "DESC":
                    tiporder_typeo = -1
                if campo != "_id":
                    key = collection.find({field: str(value)}).sort(
                        order_field, order_type)
                else:
                    key = collection.find({field: ObjectId(value)}).sort(
                        order_field, order_type)
            else:
                if field != "_id":
                    key = collection.find({field: str(value)})
                else:
                    key = collection.find({field: ObjectId(value)})

            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
                message = "Busqueda en colección "+collection_name
                result = key_json, 200
            else:
                message = "Id no encontrado en colección "+collection_name
                result = {"message": "Id no encontrado", "code": 204}, 204
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        return result

    def find_key_logic(self, collection_name, field, value):
        """Función que busca y retorna un registro

        Función que busca en una colección por medio de un campo y retorna el resultado

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            field (str): Campo por el que se buscara el registro
            value (str): Valor que se buscará en el registro
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            InvalidId: Id invalido para formato BSON
        """
        try:
            key_json = {}
            collection = self.db[collection_name]
            exclude = {
                "isDeleted": 0,
                "createdOn": 0,
                "createdBy": 0,
                "modifiedOn": 0,
                "modifiedBy": 0
            }

            if field != "_id":
                key = collection.find(
                    {field: str(value), "isDeleted": False}, exclude)
            else:
                key = collection.find(
                    {field: ObjectId(value), "isDeleted": False}, exclude)

            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
                message = "Busqueda en colección "+collection_name
                result = key_json, 200
            else:
                message = "Id no encontrado en colección "+collection_name
                result = {"message": "Id no encontrado", "code": 204}, 204

        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result
    
    def insert_action_logic(self, mongo_model, collection_name, data, id, mongo_model_user):
        """Función que inserta un elemento a una colección dada

        Función que inserta un registro en una colección especifica, se agregan
        los datos lógicos (isDeleted, createdOn, createdBy, modifiedOn, modifiedBy)

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            collection_name (str): Colección en la que se buscarán los registros
            data (dict): Datos en formato JSON que se insertarán
            id (str): Id del usuario que ejecutó la inserción
            mongo_model_user (Class): Clase de modelo de usuario para hacer el registro de la acción
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            CollectionInvalid: Si la validación de la colección falla
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            WriteError: Si ocurre un problema de escritura en DB
        """
        data['isDeleted'] = False
        data['createdBy'] = id
        data['createdOn'] = datetime.now()
        data['modifiedBy'] = None
        data['modifiedOn'] = None
        if "_id" in data['details']:
            data['details']['_id'] = data['details']['_id']['$oid']
        try:
            # Filtro de usuario 
            filter_user = { "_id": ObjectId(id) }  
            newvalues_user = { "$set": { 'last_action': data['action'] } } 
            # Update last_action in User collection 
            mongo_model_user.update_one(filter_user, newvalues_user)
            collection = self.db[collection_name]
            id_insert = mongo_model.insert_one(data).inserted_id
            key = collection.find({"_id": ObjectId(id_insert)})
            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
            result = key_json,200
            message= "Acción registrada"
        except errors.CursorNotFound as e:
            message = str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except errors.CollectionInvalid as e:
            message = str(e)
            result = {"message": "CollectionInvalid", "code": 409}, False
        except errors.DuplicateKeyError as e:
            message = str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, 204
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 409}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        return result
    
    def increment_key(self, mongo_model, collection_name, id_update, update_key):
        """Función que incrementa un valor entero en documento

        Función que incrementa un valor entero de una key en documento Mongo

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer el incremento
            collection_name (str): Colección en la que se buscarán los registros
            id_update (str): ID del elemento donde se hara la modificación de incremento
            update_key (str): Nombre de la key en documento para hacer incremento
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            InvalidId: ID invalido para formato BSON
        """
        try:
            exclude = {}
            exclude['isDeleted'] = 0
            exclude['createdOn'] = 0
            exclude['createdBy'] = 0
            exclude['modifiedOn'] = 0
            exclude['modifiedBy'] = 0
            collection = self.db[collection_name]
            myquery = {"_id": ObjectId(id_update)}
            key = self.db[collection_name].find(myquery)
            if key.count() > 0:
                newvalues = {"$inc": {update_key:1}}
                borrado = mongo_model.update_one(myquery, newvalues)
                key_find = collection.find(myquery,exclude)
                if key_find.count() > 0:
                    key_dumps = dumps(key_find)
                    key_json = json.loads(key_dumps)
                result = key_json[0],200
                message = "Actualización en colección "+collection_name
            else:
                message = "Id no encontrado en colección "+collection_name+", id "+id_update
                result = {"message": "Id no encontrado", "code": 204}, 204
        except errors.DuplicateKeyError as e:
            message = str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "El id ingresado no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result
    
    def find_if_foreign_exists(self, collection_name, list_find, key_name = "_id"):
        """Función que busca si existe una lista de elementos en una colección

        Función que busca si existe una lista de elementos "foraneos" en una
        colección dada por el usuario, deben de existir todos los elementos
        de forma contraria se regresa un error

        Args:
            collection_name (str): Colección en la que se buscarán los registros
            list_find (list): Lista de elementos a buscar si existen en colección
            key_name (str): Nombre de la key para hacer la busqueda
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            InvalidId: ID invalido para formato BSON
        """
        try:
            # Se inicializa find de elementos vacio
            find_elements = {"$and":[{"$or":[]}]}
            # Llena find de mongo con la lista de registros a buscar
            for element in list_find:
                # Si la busqueda es por _id se arma BSON
                if key_name == "_id":
                    object_element = { key_name : ObjectId(element) }
                # Si no es por _id se arma JSON
                else:
                    object_element = { key_name : element }
                # Agrega objeto a la busqueda
                find_elements['$and'][0]['$or'].append(object_element)
            # Hacemos la busqueda de elementos
            result_find = self.find_key_dynamic_logic(collection_name = collection_name,
                filter_mongo = find_elements)
            # Si encontro elementos
            if result_find[1] == 200:
                found_elements = []
                # Recorremos los elementos encontrados para llenar lista
                for result_element in result_find[0]:
                    # Si la busqueda es por _id se agrega id a arreglo
                    if key_name == "_id":
                        found_elements.append(result_element['_id']['$oid'])
                    # Si no es por _id se agrega elemento a arreglo
                    else:
                        found_elements.append(result_element[key_name])
                # Si la longitud de encontrados es diferente a la de busqueda
                if len(result_find[0]) != len(list_find):
                    # Buscamos cuales son los elementos que no se encontraron
                    residuary = (set(list_find)).difference(set(found_elements))
                    message = f"No se encontraron elementos {residuary}"
                    result = message, 204
                # Si la longitud de encontrados es igual a la de busqueda
                else:
                    message = "Los elementos buscados existen en base de datos"
                    result = message, 200
            # Si la consulta salio mal, no se encontro ni una coincidencia
            else:
                message = "No se encontraron elementos que coincidieran con busqueda"
                result = message, 204

        except errors.DuplicateKeyError as e:
            message = str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, False
        except InvalidId as e:
            message = str(e)
            result = {"message": "Alguno de los ids ingresados no es valido", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        
        return result

    def insert_one_logic_out_context(self, mongo_model, collection_name, data, id, exclude_mongo={}):
        """Función que inserta un elemento logicamente a una colección dada

        Función que inserta un registro en una colección especifica, se agregan
        los datos lógicos (isDeleted, createdOn, createdBy, modifiedOn, modifiedBy)

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            collection_name (str): Colección en la que se buscarán los registros
            data (dict): Datos en formato JSON que se insertarán
            id (str): Id del usuario que ejecutó la inserción
            exclude_mongo (dict): Valores que se excluirán de la consulta (exclude)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            CursorNotFound: Si no se encuentra el cursor de la base de datos
            CollectionInvalid: Si la validación de la colección falla
            DuplicateKeyError: Si el campo a ingresar ya existe en DB
            WriteError: Si ocurre un problema de escritura en DB
        """
        created_date=datetime.now()
        data['isDeleted'] = False
        data['createdBy'] = id
        data['createdOn'] = created_date
        data['modifiedBy'] = None
        data['modifiedOn'] = None
        exclude_mongo['isDeleted'] = 0
        exclude_mongo['createdOn'] = 0
        exclude_mongo['createdBy'] = 0
        exclude_mongo['modifiedOn'] = 0
        exclude_mongo['modifiedBy'] = 0
        try:
            collection = self.db[collection_name]
            id_insert = mongo_model.insert_one(data).inserted_id
            key = collection.find({"_id": ObjectId(id_insert)},exclude_mongo)
            if key.count() > 0:
                key_dumps = dumps(key)
                key_json = json.loads(key_dumps)
            message='Inserción en colección '+collection_name+", id "+str(id_insert)
            result = key_json[0],200
        except errors.CursorNotFound as e:
            message=str(e)
            result = {"message": "CursorNotFound", "code": 409}, False
        except errors.CollectionInvalid as e:
            message=str(e)
            result = {"message": "CollectionInvalid", "code": 409}, False
        except errors.DuplicateKeyError as e:
            message=str(e)
            result = {"message": "DuplicateKeyError", "code": 204}, 204
        except errors.WriteError as e:
            message=str(e)
            result = {"message": "WriteError", "code": 409}, False
        except Exception as e:
            message=str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        logger_out_context = setup_logger(name = "api_log",
            log_file = "logs/api_log.log",
            level = logging.INFO)

        # LOGS
        if result[1]==200:
            logger_out_context.info(message)
        elif result[1]==204:
            logger_out_context.warning(message)
        else:
            logger_out_context.error(message)
        return result

    def update_array(self,mongo_model,collection_name,object_id,name_key,find_key_array,find_key_id,data,user_id,exclude_mongo={}):
        """Actualiza los datos de un arreglo de un documento

        Función que actualiza los datos de un arreglo de un documento
        y actualiza los datos de modificación

        Args:
            mongo_model (Class): Clase de modelo de mongo para hacer la inserción
            collection_name (str): Colección en la que se modificara
            object_id (str): Id del documento a modificar
            name_key (str): Clave en la cual se insertarán los datos
            find_key_array (str): Llave para busqueda en el arreglo
            find_key_id (str): Identicador de busqueda en el arreglo
            data (dict): Valores que se insertarán al documento
            user_id (str): Id del usuario que ejecutó la acción
            exclude_mongo (dict): Valores que se excluirán de la consulta (exclude)
        
        Returns:
            [tuple]: Result (Dict) , 200|204|False

        Exceptions:
            WriteError: Si ocurre un problema de escritura en DB
        """
        exclude_mongo['isDeleted'] = 0
        exclude_mongo['createdOn'] = 0
        exclude_mongo['createdBy'] = 0
        exclude_mongo['modifiedOn'] = 0
        exclude_mongo['modifiedBy'] = 0
        mod = {name_key+".$":data}
        mod2={"modifiedOn": datetime.now()}
        if user_id!="N/A" and user_id!=None and user_id!="":
            mod["modifiedBy"]=user_id
        findId = {"_id": ObjectId(object_id),name_key+"."+find_key_array:find_key_id}
        updateQuery = {"$set": mod}
        updateQuery2 = {"$set": mod2}
        try:
            collection = self.db[collection_name]
            update = mongo_model.update_one(findId, updateQuery)
            if update.modified_count == 0 or update.modified_count == None:
                result = {"message": "No hubo modificaciones", "code": 204}, 204
                message = "No hubo modificaciones en colección "+collection_name+", id "+object_id
            else:
                myquery={"_id": ObjectId(object_id)}
                update = mongo_model.update_one(findId, updateQuery2)
                key_find = collection.find(myquery,exclude_mongo)
                if key_find.count() > 0:
                    key_dumps = dumps(key_find)
                    key_json = json.loads(key_dumps)
                result = key_json[0],200
                message = "Modificaciones en arreglos, colección "+collection_name+", id "+object_id
        except errors.WriteError as e:
            message = str(e)
            result = {"message": "WriteError", "code": 204}, False
        except Exception as e:
            message = str(e)
            result = {"message": "Ocurrio un error inesperado", "code": 409}, False
        
        return result
