# Documentación Decoradores:
# https://recursospython.com/guias-y-manuales/decoradores/
# Flask libraries
from flask import request
from flask_restplus import fields
# 3rd party libraries
from flask_jwt_extended import (create_access_token, get_jwt_identity)
# Standard/core python libraries
from functools import wraps
from bson.objectid import ObjectId
from urllib.parse import urlparse
import json
import jwt
# Our custom libraries
from core.utils import Tools
from core.database import Connector
from core.models import UserSessions as mongo_model


def valid_values_empty(keys_to_valid):
    """
            Verifica que las claves del json recibido se encuentren en el arreglo
            pasado como paremtro, y que al mismo tiempo el valor no contenga datos vacio.
            De no ser así, retorna un mensaje de error e interrumpe el flujo.

            Parámetros:
            keys_to_valid		--		[Array] 		--		  Valores para validar en el json.

    """
    def validator(func):
        @wraps(func)
        def wrapper(*args, **kwds):
            json_data = request.get_json(force=True)

            for (key, value) in json_data.items():
                if isinstance('string', type(value)):
                    value = value.replace(' ', '')
                    if (key in keys_to_valid) and (value == ''):
                        message = "El campo '" + key + "' no puede estar vacio"
                        return Tools.response_maker_1(message, {}, 204, None)
            return func(*args, **kwds)
        return wrapper
    return validator


def valid_keys_required(model_to_valid):
    """
            Comprueba en que el JSON enviado no existan campos de más comparado con el modelo.
            Del modelo extrae los campos que son requeridos y comprueba que
            exista en el json recibido, de lo contrario retorna un error e interrumpe el flujo.
            Posteriormente comprueba que los valores del json recibido sean del mismo tipo
            que los campos del modelo, de lo contrario retorna un error e interrumpe el flujo.
    """
    def validator(func):
        @wraps(func)
        def wrapper(*args, **kwds):
            json_data = request.get_json(force=True)

            for key in json_data:
                if key not in model_to_valid:
                    message = "El campo '" + key + "' no está permitido."
                    return Tools.response_maker_1(message, {}, 204, None)
            for (key, value) in model_to_valid.items():
                if (key not in json_data) and (value.required):
                    message = "'" + str(key) + "' es requerido como parámetro."
                    return Tools.response_maker_1(message, {}, 204, None)
                elif key in json_data:
                    valueJson = json_data.get(key)
                    if 'type' in value.__schema__:
                        modelType = value.__schema__['type']
                    else:
                        modelType = value.model
                        value.campos = value.model
                    if isinstance(modelType, type(dict())):
                        for campo in value.campos:
                            if campo not in valueJson:
                                message = "'{0}' es requerido como parámetro. Dentro de '{1}'".format(
                                    str(campo), str(key))
                                return Tools.response_maker_1(message, {}, 204, None)
                    if modelType == 'integer':
                        modelType = int()
                    elif modelType == 'number':
                        modelType = float()
                    elif modelType == 'boolean':
                        modelType = bool()
                    elif modelType == 'array':
                        modelType = list()
                    elif modelType == 'object':
                        modelType = dict()
                    elif modelType == None and isinstance(value, fields.Nested):
                        modelType = dict()
                    if not isinstance(modelType, type(valueJson)):
                        message = "'" + \
                            str(valueJson) + "' no es un tipo de dato valido '" + \
                            str(modelType) + "' "

                        return Tools.response_maker_1(message, {}, 204, None)
            return func(*args, **kwds)
        return wrapper
    return validator


def check_session(funcion_parametro):
    """
            Este decorador revisa si el usuario que está realizando la acción
            de login ya posee un session activa, de ser así, revoca el Token
            activo y genera uno nuevo.
    """

    def funcion_interior(*args, **kwargs):

        # acciones
        data = {
            "isDeleted": True,
        }
        conex = Connector()
        json_data = request.get_json(force=True)
        query = conex.find_key_dynamic_logic(
            "user_sessions", {"email": json_data['email']})
        result = conex.authenticate("users", json_data)
        if query[1] != 200 or result[1] != 200:
            conex.close()
            return funcion_parametro(*args, **kwargs)
        elif result[1] == 204:
            mensaje = "Email y/o contraseña incorrectos"
            result = {}
            return Tools.response_maker_1(mensaje, result, 204, None)
        else:
            userid = conex.find_key_dynamic_logic(
                'users', {"email": json_data['email']})
            del userid[0][0]['is_active']
            del userid[0][0]['password']
            # Elimnacion de magen temporalmente
            try:
                photo = result[0]['photo']
                result[0]['photo'] = ''
            except:
                pass

            #revoacion_token = conex.edit_only_keys_logic(mongo_model,query[0][0]['_id']['$oid'],data,userid[0][0]['_id']['$oid'])
            class_request = str(args[0])
            if class_request.startswith('<apis.login.LoginQ'):
                for q in query[0]:
                    if "q" in q:
                        revoacion_token = conex.edit_only_keys_logic(
                            mongo_model, q['_id']['$oid'], data, userid[0][0]['_id']['$oid'])
                        jti_revocado = q['jti']
            else:
                for q in query[0]:
                    if "q" not in q:
                        revoacion_token = conex.edit_only_keys_logic(
                            mongo_model, q['_id']['$oid'], data, userid[0][0]['_id']['$oid'])
                        jti_revocado = q['jti']
            #revoacion_token = conex.edit_only_keys_logic(mongo_model,query[0][0]['_id']['$oid'],data,userid[0][0]['_id']['$oid'])
            token = funcion_parametro(*args, **kwargs)
            token = token.get_json(force=True)
            message = "Inicio de sesión exitoso, el usuario tenía una sesión activa la cual ha sido revocada"
            #userid[0][0]['jti_revocado'] = query[0][0]['jti']
            userid[0][0]['jti_revocado'] = jti_revocado
            userid[0][0]['token'] = token['payload']['token']

            # OBTENER GRUPO DE USUARIO
            result = userid[0]
            id_user = result[0]['_id']['$oid']
            filtro = {"_id": ObjectId(id_user)}
            lista_foraneo = []
            objeto_f = {"coleccion": "groups",
                        "campo": "group", "tipo": "unico"}
            lista_foraneo.append(objeto_f)
            conex = Connector()
            result_user_group = conex.get_all("users", lista_foraneo, filtro)

            # OBTENER PERMISOS DE GRUPO
            group_id = result_user_group[0][0]['group']['_id']['$oid']
            filtro2 = {"_id": ObjectId(group_id)}
            lista_foraneo2 = []
            objeto_f2 = {"coleccion": "permissions",
                         "campo": "permissions", "tipo": "lista"}
            lista_foraneo2.append(objeto_f2)
            result_user_group_permission = conex.get_all(
                "groups", lista_foraneo2, filtro2)

            # AGREGA PERMISOS A GRUPO DE USUARIO
            result[0]['group'] = result_user_group_permission[0][0]
            userid[0][0]['group']

            try:
                userid[0][0]['photo'] = photo
            except:
                pass
            return Tools.response_maker_1(message, userid[0][0], 200, None)

    return funcion_interior


def add_extra_data(func):
    '''Obtiene todos los clientes registrados'''
    def wrapper(*args, **kwds):
        # # Para domain name simple
        # o = urlparse(request.base_url)
        # host_simple = o.hostname
        # # Para domain completo
        # host_completo = request.host_url
        # access_token = create_access_token(identity = "System")
        #
        # payload  = {}
        # headers = {
        #   'Content-Type': 'application/x-www-form-urlencoded',
        #   'Authorization': 'Bearer ' + access_token
        # }
        '''Obtiene todos los clientes registrados'''
        resultado = func(*args, **kwds)
        resultado = resultado.get_json(force=True)
        for cliente in resultado['payload']['clients']:
            cliente_id = cliente['_id']['$oid']
            conex = Connector()
            # PARA TOTAL AGENTES
            agentes = conex.find_key_logic("agents", "client", cliente_id)
            if agentes[1] != 200:
                cliente.update({"agentes": 0})
                cliente.update({"interacciones": 0})
                cliente.update({"sesiones": 0})
            else:
                cliente.update({"agentes": len(agentes[0])})
                # PARA TOTAL INTERACCIONES / SESIONES
                for agente in agentes[0]:
                    agente_id = agente['_id']['$oid']
                    interacciones = conex.find_key(
                        "interacciones", "agente_id", agente_id)
                    if interacciones[1] != 200:
                        cliente.update({"interacciones": 0})
                        cliente.update({"sesiones": 0})
                    else:
                        cliente.update(
                            {"interacciones": len(interacciones[0])})
                        sesiones = []
                        for interaccion in interacciones[0]:
                            sesiones.append(
                                interaccion['queryParams']['payload']['sessionId'])
                        sesiones = set(sesiones)
                        cliente.update({"sesiones": len(sesiones)})

        return resultado
    return wrapper


def validate_permissions(permissionList):
    """
            Verifica que el usuario que realiza la petición cuente con los permisos necesarios.

            Parámetros:
            permissionList		--		[LIst] 		--		  Lista de nombre de permisos.

    """
    def validator(foo):
        def wrapper(args, **kwargs):
            if len(permissionList) == 0:
                return foo(args, **kwargs)

            idsList = getIdPermissions(permissionList)
            token = get_jwt_identity()
            try:
                userId = token['_id']['$oid']
            except:
                userId = token[0][0]

            conex = Connector()
            userQuery = conex.find_key_dynamic_logic(
                'users', {"_id": ObjectId(str(userId))})
            if userQuery[1] != 200:
                if userQuery[1] == 204:
                    return Tools.response_maker_1("Usuario no encontrada", {}, 204)
                else:
                    mensaje = userQuery[0]['message']
                    return Tools.response_maker_1(mensaje, {}, userQuery[0]['code'], None)
            groupQuery = conex.find_key_dynamic_logic(
                'groups', {"_id": ObjectId(str(userQuery[0][0]['group']))})
            if groupQuery[1] != 200:
                if groupQuery[1] == 204:
                    return Tools.response_maker_1("Grupo no encontrado", {}, 204)
                else:
                    mensaje = groupQuery[0]['message']
                    return Tools.response_maker_1(mensaje, {}, groupQuery[0]['code'])
            conex.close()
            for id in idsList:
                if not id in groupQuery[0][0]['permissions']:
                    return Tools.response_maker_1("No cuentas con los permisos necesarios para realizar esta operación", {}, 204)
            return foo(args, **kwargs)
        return wrapper
    return validator


def getIdPermissions(permissionList):
    idsList = []
    conex = Connector()
    for permission in permissionList:
        result = conex.find_key_dynamic_logic(
            'permissions', {"name": permission})
        if result[1] == 200:
            idsList.append(result[0][0]['_id']['$oid'])
    return idsList


def format_templates_agentes(func):
    def wrapper(*args, **kwds):
        '''Obtiene los nombres de los templates, limpia la data'''
        resultado = func(*args, **kwds)
        resultado = resultado.get_json(force=True)
        _templates = []
        _template_query = []
        for data in resultado['payload']['templates']:
            id_plantilla = data['id_intencion']['id_plantilla']
            _templates.append(id_plantilla)
        _templates = list(dict.fromkeys(_templates))
        conex = Connector()
        for data in _templates:
            # CONSULTO EL TEMPLATE
            template = conex.find_key_logic("templates", "_id", data)
            json = {
                "id_template": template[0][0]['_id']['$oid'],
                "nombre_plantilla": template[0][0]['nombre_plantilla']
            }
            _template_query.append(json)
        del resultado['payload']['templates']
        new = {
            "templates": _template_query
        }
        resultado['payload'].update(new)
        return resultado
    return wrapper
