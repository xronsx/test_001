# -*- coding: utf-8 -*-
# Flask libraries
from flask import make_response
from flask import current_app
# 3rd party libraries
from flask_mail import Mail, Message
from pymongo import MongoClient, errors
# Standard/core python libraries
import json
import os
# Our custom libraries


class Tools:

	def response_maker(resultado, code, nombre_campo):
		if nombre_campo == None:
			if code == 204:  # Es un error y tiene código
				try:
					resp = make_response(
						{'payload': {'token': resultado.code}})
					resp.headers.extend(
						{'code': code, 'message': str(resultado)})
				except:  # a veces envio un json como objeto de error
					resultado = json.loads(str(resultado))
					resp = make_response(
						{'payload': {'token': str(resultado['code'])}})
					resp.headers.extend(
						{'code': code, 'message': str(resultado['message'])})
			# Es un OK o un error sin código (500, 409)
			elif (code == 200 or (code != 200 and code != 204)):
				resp = make_response({'message': str(resultado)})
				resp.headers.extend({'code': code, 'message': str(resultado)})
		else:  # Se ejecutó una busqueda (find)
			# El campo será un array (ese wey pidió que se lo entregaran así)
			resp = make_response({"payload": {nombre_campo: resultado}})
			resp.headers.extend({'code': code})

		return resp

	"""
		{
			headerResponse: {
				message: "Exito al consultar",
				code: 200
			},
			payload: {
				token: 1231231239812798123h9j1293812,
				username: asdasd
				email: email@email.com
				Nombre: Alejandro
			}
		}
	"""
	def response_maker_1(message, result, code, field_name=None):
		"""Función encargada de armar respuesta como lo pide FrontEnd

		Args:
			message (str): Mensaje de respuesta en JSON
			result (dict): Cuerpo de respuesta en JSON
			code (int): Código de respuesta en JSON
			field_name (str, optional): Nombre de campo donde se regresa respuesta. Defaults to None.

		Returns:
			[make_response]: Respuesta formada en formato FrontEnd
		"""
		if field_name == None:
			response = make_response({
				"headerResponse": {
					"message": message,
					"code": code
				},
				"payload": result
			})
		else:  # Se ejecutó una busqueda (find)
			response = make_response({
				"headerResponse": {
					"message": message,
					"code": code
				},
				"payload": {
					field_name: result
				}
			})
		return response

	def valid_values_empty(keys, json_data):
		"""Función que verifica que claves no tengan valores vacios

		Función que recibe las claves que no deben estár vacias, y el json que
		contiene toda la información y valida que las llaves enviadas no estén
		vacias en el JSON.

		Parámetros:
		keys                --  [List]      --  Nombres de las claves a validar
		json_data           --  [JSON]      --  Datos en formato JSON a validar

		"""
		for (key, value) in json_data.items():
			if (key in keys) and (value == ''):
				return {
					'message': 'El campo ' + key + ' no puede estar vacio',
					'code': 409
				}, 409

		return "OK", 200

	def send_email(titulo, destinatarios, mensaje):
		"""Función que envia emails a uno o más destinatarios

		Función que envia correos a uno o más destinatarios, con título y cuerpo
		del mensaje


		Parámetros:
		titulo              --  [String]    --  Colección en la que se buscara el registro
		destinatarios       --  [List]      --  Campo por el que se buscara el registro
		mensaje             --  [String]    --  Valor que se buscará en el registro

		"""
		try:
			mail = Mail(current_app)
			msg = Message(
				titulo, sender=current_app.config['DEFAULT_SENDER'], recipients=destinatarios)
			msg.html = mensaje
			mail.send(msg)
			return "OK", 200
		except:
			return "Error al enviar mensaje", False

	def database_connection():
		CONNECTION_STRING = os.environ.get('CONNECTION_STRING', "")
		MONGO_HOST_LOCAL = os.environ.get('MONGO_HOST_LOCAL', "localhost")

		if CONNECTION_STRING != '' and CONNECTION_STRING != None:
			CONNECTION_STRING = os.environ.get('CONNECTION_STRING')
			mongoClient = MongoClient(CONNECTION_STRING)
			dbtype = "atlas"
		elif MONGO_HOST_LOCAL != '' and MONGO_HOST_LOCAL != None:
			mongoClient = MongoClient(host=os.environ.get('MONGO_HOST_LOCAL', "localhost"),
								port=int(os.environ.get('MONGO_PORT_LOCAL', "27017")))

			dbtype = "localhost"
		else:
			mongoClient = MongoClient(host=os.environ.get('MONGO_HOST'),
									port=(os.environ.get('MONGO_PORT')),
									username=os.environ.get('MONGO_USER'),
									password=os.environ.get('MONGO_PASSWORD'))
			dbtype = "EC2"
		print(" * Connected " + dbtype)
		return mongoClient, dbtype

	def customDict(dictObj, listFields):
		"""
		Función que extrae los valores datos de un diccionario u objeto

		Parámetros:
		dictObj             --  [Dict/Obj]  --  Diccionario u objeto a modificar
		listFields           --  [List]      -- Campos a extraer

		"""
		if not isinstance(listFields, list) or len(listFields) == 0:
			return dictObj
		newDict = {}
		if isinstance(dictObj, dict):
			for field in listFields:
				if field in dictObj:
					newDict[field] = dictObj[field]
			return newDict
		elif isinstance(dictObj, object):
			for field in listFields:
				if hasattr(dictObj, field):
					newDict[field] = getattr(dictObj, field)
					return namedtuple('CustomObject', newDict.keys())(*newDict.values())
