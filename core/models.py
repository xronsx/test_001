# -*- coding: utf-8 -*-
# Flask libraries
# 3rd party libraries
from pymongo import IndexModel
from pymongoext import *
# Standard/core python libraries
from datetime import datetime
import os
# Our custom libraries
from .database import Connector
from core.utils import Tools

class Tokens(Model):
    __collection_name__ = "tokens"
    @classmethod
    def db(cls):
        conn = Connector()
        db = conn.db
        return db

    __schema__ = DictField(
        dict(
            token=StringField(required=True),
            fecha_expiracion=StringField(required=False),
            id_user=StringField(required=False),
        )
    )

    __indexes__ = [IndexModel('token', unique=True)]


class UserSessions(Model):
    __collection_name__ = "user_sessions"
    @classmethod
    def db(cls):
        conn = Connector()
        db = conn.db
        return db

    __schema__ = DictField(
        dict(
            email=StringField(required=True),
            jti=StringField(required=True),
        ))

class Products(Model):
    __collection_name__ = "products"
    @classmethod
    def db(cls):
        conn = Connector()
        db = conn.db
        return db

    __schema__ = DictField(
        dict(
            name=StringField(required=True),
            category=StringField(required=True),
            description=StringField(required=True),
            price=FloatField(required=True),
            quantity=NumberField(required=True),
            inventory=StringField(required=True),
            image=StringField(required=True),
        ))
    
    __indexes__ = [IndexModel('name', unique=True)]

